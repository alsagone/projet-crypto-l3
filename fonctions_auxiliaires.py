# Hakim ABDOUROIHAMANE - L3 S5 Informatique

import sys
import collections


def pad_bits(bits_str):
    """Ajoute du padding à une suite de bits pour qu'elle ait une longueur qui soit un multiple de 8.
    """
    mod = len(bits_str) % 8
    longueur_padding = 8 - mod if (mod > 0) else 0
    return "0" * longueur_padding + bits_str


def int_to_bin(x):
    """Transforme l'entier passé en paramètre en binaire"""
    return pad_bits(bin(x)[2:])


def decouper_blocs(string, taille_bloc):
    """Découpe la string passée en paramètre en des blocs de taille définie"""
    return [string[i:i+taille_bloc] for i in range(0, len(string), taille_bloc)]


def bin_to_string(bits_str):
    """Transforme la suite de bits passée en paramètre en texte"""
    # On ajoute du padding si le nombre de bits n'est pas multiple de 8
    bits_str = pad_bits(bits_str)

    # On découpe la string en blocs de 8 bits
    blocs = decouper_blocs(bits_str, 8)

    # On transforme chaque bloc en int - puis on cherche le caractère correspondant au code ASCII
    lettres = [chr(int(b, 2)) for b in blocs]

    return "".join(lettres)


def string_to_bin(string):
    """Transforme la string passée en paramètre en une suite de bits"""
    bits = []
    for char in string:
        # On cherche le code ASCII du caractère, on le convertit en binaire
        b = int_to_bin(ord(char))

        # Et on l'ajoute à notre chaine de bits
        bits.append(b)

    return "".join(bits)


def decomposition_facteurs_premiers(n):
    """Renvoie la décomposition en facteurs premiers de l'entier passé en paramètre"""
    facteurs = []
    i = 2

    while (i*i <= n):
        if (n % i == 0):
            facteurs.append(i)
            n //= i

        else:
            i += 1

    if n > 1:
        facteurs.append(n)

    return facteurs

# On compte le nombre de fois qu'un facteur intervient dans la décomposition
# On aboutit à un dict qui associe le nombre et son exposant
# Exemple: decomposition(248) = [2, 2, 2, 31]
# d = {2: 3, 31: 1}
# Donc phi(248) = (2^3 - 2^2) * (31^1 - 31^0) = 120


def phi(n):
    """Calcule l'indicatrice d'Euler de l'entier passé en paramètre"""
    d = collections.Counter(decomposition_facteurs_premiers(n))

    p = 1

    for (nombre, exposant) in d.items():
        p *= pow(nombre, exposant) - pow(nombre, exposant-1)

    return p


def inverse_modulaire(n, m):
    """Calcule l'inverse de n modulo m"""
    inv = -1
    try:
        inv = pow(n, -1, m)

    except:
        print(f"L'inverse de {n} modulo {m} n'existe pas.", file=sys.stderr)
        sys.exit(1)

    return inv


def logarithme_discret(a, m, x):
    """Renvoie (s'il existe) le plus petit entier b tel que a^b % m = x"""
    s = 1
    i = 0
    b = -1
    trouve = False

    while not(trouve) and (i < m):
        s = (s * a) % m

        if (s == x):
            b = i + 1
            trouve = True

        else:
            i += 1

    if not(trouve):
        print(
            f"Il n'existe aucun entier b tel que {a}^b % {m} = {x}", file=sys.stderr)
        sys.exit(1)

    return b
