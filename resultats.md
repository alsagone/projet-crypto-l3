Hakim ABDOUROIHAMANE - L3 S5 Informatique

### Partie 1

Certificat de Bob déchiffré

> Bob est bien celui qu'il pretend etre, et sa cle publique est (e,n)=(1639,1618108771) en decimal

$d_{bob} = 546899671$

### Partie 2

$a = 2797$
$g^{ab} = 33406556$
$clé_{déchiffrement} = 57607489$

Message de Bob

> Rendez-vous a 16h45 au point de latitude Nord 48.5800, longitude Est 7.6800

Ville: Eckbolsheim

Message modifié

> Rendez-vous a 15h45 au point de latitude Nord 48.5815, longitude Est 7.7503

Message modifié chiffré

> 194287964,146401930, 30842062, 89643614, 56316760,
> 1916587, 217361974, 230484859, 153450182, 205738073,
> 36498386, 124488166, 147333803, 6856974, 199362291,
> 53554277, 178981588, 108540160, 121860464, 34970989,
> 147333803, 192475604, 154710676, 111932126, 85592705
