# Hakim ABDOUROIHAMANE - L3 S5 Informatique

import fonctions_auxiliaires as aux
import sys

p = 243138817
g = 39534652
g_a = 110309364
g_b = 180735528
g_ab = -1
cle_dechiffrement = -1


def trouver_cle_dechiffrement():
    global p
    global g
    global g_a
    global g_b
    global g_ab

    # On pose X = g^a, on connait g
    # Donc on doit trouver a tel que g^a = X mod p
    a = aux.logarithme_discret(g, p, g_a)

    # On connait a donc on peut trouver la valeur de g^(ab) modulo p
    g_ab = pow(g_b, a, p)

    # La clé de déchiffrmeent c'est l'inverse de g^(ab) modulo p
    c = pow(g_ab, -1, p)

    print(f'a = {a}')
    print(f'g_ab = {g_ab}')
    print(f'clé déchiffrement = {c}\n')
    return c


def dechiffrer_message(message_chiffre):
    global p
    global cle_dechiffrement

    # Pour éviter de recalculer la clé si on la connait déjà
    if cle_dechiffrement == -1:
        cle_dechiffrement = trouver_cle_dechiffrement()

    # Pour décoder un nombre X, on multiplie X par la clé, le tout modulo p
    message_dechiffre = [(cle_dechiffrement * nombre) % p
                         for nombre in message_chiffre]

    message_clair = ""

    # Pareil que la partie 1, on convertit le nombre en binaire, puis en texte
    for nombre in message_dechiffre:
        b = aux.int_to_bin(nombre)
        message_clair += aux.bin_to_string(b)

    return message_clair


def chiffrer_message(message):
    global p
    global g_ab

    message_crypte = []

    # On découpe le message par blocs de 3 caractères
    for bloc in aux.decouper_blocs(message, 3):

        # On transforme chaque bloc en une suite de bits
        bits = aux.string_to_bin(bloc)

        # On transforme la suite de bits en un nombre décimal
        # Puis on chiffre le nombre obtenu en le multipliant par g_ab, le tout modulo p
        nombre = (int(bits, 2) * g_ab) % p

        message_crypte.append(nombre)

    return message_crypte


def message_string(message):
    """Renvoie les nombres du message dans une string - séparés par des virgules"""
    return ",".join([str(nombre) for nombre in message])


def partie_2():
    message_chiffre = [194287964, 146401930, 30842062, 89643614, 56316760, 162062335, 217361974, 230484859, 153450182, 205738073, 36498386, 124488166,
                       147333803, 6856974, 199362291, 53554277, 178981588, 30434524, 121860464, 34970989, 147333803, 192475604, 154710676, 78525570, 72671464]

    message_bob = dechiffrer_message(message_chiffre)
    print(f"Message Bob: {message_bob}\n")

    message_modifie_clair = "Rendez-vous a 15h45 au point de latitude Nord 48.5815, longitude Est 7.7503"
    message_modifie_chiffre = chiffrer_message(message_modifie_clair)

    print("Test si le nouveau message est correctement chiffre: ", end="")
    message_modifie_dechiffre = dechiffrer_message(message_modifie_chiffre)

    if (message_modifie_clair == message_modifie_dechiffre):
        print("OK !\n")

    else:
        print("Erreur.")
        print(
            f'message_modifie_clair: "{message_modifie_clair}"', file=sys.stderr)
        print(
            f'dechiffrer(message_modifie_dechiffre): {message_modifie_dechiffre}', file=sys.stderr)
        sys.exit(1)

    message_modifie_chiffre = message_string(message_modifie_chiffre)
    print(f"Message modifié chiffré\n{message_modifie_chiffre}")


if __name__ == "__main__":
    partie_2()
