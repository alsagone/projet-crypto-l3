Voici le challenge :
Bob veut envoyer un message à Alice de manière sécurisée, et vous devez intercepter le message qu'envoie Bob, altérer ce message et envoyer ce message altéré en se faisant passer pour Bob.

Les détails :
Bob envoie à Alice son certificat, chiffré avec la clé privée d'une autorité de certification AC en utilisant un cryptosystème RSA.
Ce certificat contient la clé publique de Bob (pour quelques détails sur les certificats, un très court résumé, ou wikipédia...)
Bob envoie aussi la partie publique $(p,g,g^a)$ d'un échange de clés Diffie-Hellman, signé avec sa clé privée.
Alice envoie sa partie publique $g^b$
Bob chiffre un message $M$ en utilisant la clé secrète partagée $g^{ab}$, en multipliant les blocs de $M$ par $g^{ab}\mod p$ (procédé d'ElGamal).
Ce message contient les coordonnées GPS d'un lieu, et une heure de rendez-vous.

Vous devez remplacer ces coordonnées par celles du parvis de la cathédrale de Strasbourg (à savoir : latitude Nord 48.5815, longitude Est : 7.7503), et enlever une heure à l'heure du rendez vous, puis envoyer le message ainsi altéré à Alice.

Vous compléterez les champs suivants et enverrez les programmes utilisés à l'adresse : ceroi@unistra.fr

Les données et les réponses :
Clé publique de l'autorité de certification : $e_{AC}=1279$, $n_{AC}=7688940817$

Certificat de Bob (en décimal, chaque nombre est le chiffrement de 3 octets correspondant à 3 caractères en ASCII) :
7222183998, 6459034778, 3075068764, 4112544264, 322860827, 4685169648, 7116217792, 7164529639, 6874397890, 7523542442, 2306658685, 6180524219, 6439154796, 4679240239, 3536261870, 2311664488, 2609293363, 420019851, 2377660672, 6459034778, 5693052903, 4616736431, 1408738508, 5298114578, 7527836852, 7634958051, 4881298898, 3534162203, 6661254256, 3996124118, 7107493649, 3151843911

Clé D.H. de Bob : $p=243138817, g=39534652, g^a =110309364$
Clé D.H. d'Alice : $g^b = 180735528$

Message chiffré de Bob : le message clair de Bob a été découpé en blocs de 3 caractères (si le dernier bloc ne contient pas 3 caractères on le compète par des octets nuls) en mettant bout à bout les 3 octets on obtient un nombre sur 24 bits, Bob a ensuite multiplié ces nombres par g^ab (modulo p), et on vous donne le résultat.
194287964,146401930,30842062,89643614,56316760,162062335,217361974,230484859,153450182,205738073,36498386,124488166,147333803,6856974,199362291,53554277,178981588,30434524,121860464,34970989,147333803,192475604,154710676,78525570,72671464

Autorité de Certification
Un problème des cryptosystème à clé publique, et qu'on ne sait pas qui envoie le message. Dans notre situation, Alice reçoit des messages de Bob, mais comment être sûr qu'ils viennent bien de Bob ?

Un méthode est d'utiliser un « tiers de confiance », un entité qui va vérifier physiquement qui est Bob, et assurer que la clé publiée prétendûment par Bob est bien celle de Bob.

Concrètement : Bob va voir l'autorité de certification avec sa clé publique $(n_B,e_B)$.

L'autorité vérifie son identité, emet une fiche $F$ qui dit quelque chose comme "la personne titulaire de ce certificat est bien Bob et sa clé publique est $(n_B=....,e_B=.....)$".

Puis elle chiffre F avec sa clé PRIVÉE $(n_{AC},d_{AC})$, on obtient $F'$.
$F$ et $F'$ sont librement diffusables.

Comment Bob certifie ses messages ?
Il envoie à Alice un message $M$ qu'il a chiffré avec sa clé privée $(n_B,e_B)$.
Alice reçoit ce message, elle le déchiffre avec la clé publique de Bob.
Bob envoie aussi sa fiche $F$ et la fiche chiffrée $F'$
Alice déchifffe alors $F'$ avec la clé publique de l'AC.
Si elle obtient $F$ , elle est sûre que $F$ et $F'$ ont été emis par l'AC, donc qu'elle peut faire confiance en les informations de cette fiche.
Elle compare alors la clé publique de la fiche $F$ avec la clé que Bob a publiée, si c'est la même alors Alice est sûre qu'il s'agit bien de la clé de Bob.
Enfin, elle déchiffre le message $M'$ de Bob grâce à cette clé publique.
Si le message est lisibie, c'est qu'il a été chiffré avec la clé privée de Bob, que seul Bob possède : donc le message vient bien de Bob.

Conclusion : le but d'un certificat est d'être déchiffré : il est donc chiffré avec la clé PRIVÉE de l'AC, afin d'être déchiffrable avec sa clé PUBLIQUE : n'essayez pas d'obtenir la clé privée de l'AC, c'est hors sujet !
