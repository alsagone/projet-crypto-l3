# Hakim ABDOUROIHAMANE - L3 S5 Informatique

import fonctions_auxiliaires as aux

# Soit x un nombre du certificat
# On déchiffre x en faisant x^e_ac mod n_ac - on aboutit à l'entier m
# On convertit m en binaire -> b
# Puis on convertit en string et on l'accole au certificat


def dechiffrer_certificat_bob():
    certificat_bob = [7222183998, 6459034778, 3075068764, 4112544264, 322860827, 4685169648, 7116217792, 7164529639, 6874397890, 7523542442, 2306658685, 6180524219, 6439154796, 4679240239, 3536261870, 2311664488,
                      2609293363, 420019851, 2377660672, 6459034778, 5693052903, 4616736431, 1408738508, 5298114578, 7527836852, 7634958051, 4881298898, 3534162203, 6661254256, 3996124118, 7107493649, 3151843911]

    n_ac, e_ac = (7688940817, 1279)

    certificat_dechiffre = ""

    for nombre in certificat_bob:
        m = pow(nombre, e_ac, n_ac)
        b = aux.int_to_bin(m)
        certificat_dechiffre += aux.bin_to_string(b)

    print(certificat_dechiffre)
    return


def partie_1():
    dechiffrer_certificat_bob()

    e_bob, n_bob = (1639, 1618108771)

    # La clé privée, c'est l'inverse de e_bob modulo phi(n_bob)
    d_bob = aux.inverse_modulaire(e_bob, aux.phi(n_bob))
    print(f"d_bob = {d_bob}")
    return


if __name__ == "__main__":
    partie_1()
